<?php

/** 
 * Holds the config array
 */ 

// -----------------------------------------------------------------------------
// DON'T EDIT:
// -----------------------------------------------------------------------------

$spctrm_cfg = array(
  'options' => array(),
  'schemes' => array(),
);

// -----------------------------------------------------------------------------
// NOW START EDITING:
// -----------------------------------------------------------------------------

/*
 *
 * The logic scheme structure is set up like this:
 * 
 * $spctrm_cfg['options']['CUSTOM_OPTION_MACHINE_NAME'] = array(
 *    'title' => 'CUSTOM TITLE',
 *    'element' => 'AFFECTED HTML ELEMENT',
 *  );
 * 
 * !!! For CUSTOM_OPTION_MACHINE_NAME you must use alphanumeric characters !!!
 * 
 * If you want to use a linear gradient use the following structure.
 * You can add as much colorstops as you want, keep incremental numeration of
 * the colorstops array elements. The colorstop values will change the gradients
 * transitions - only use percentages.
 * 
 * Also see: https://developer.mozilla.org/en-US/docs/CSS/linear-gradient
 * 
 * $spctrm_cfg['options']['CUSTOM_OPTION_MACHINE_NAME'] = array(
 *  'title' => 'CUSTOM TITLE',
 *  'element' => 'AFFECTED HTML ELEMENT',
 *  'gradient' => array (
 *    'meta' => array(
 *      'type' => 'linear',
 *      'to' => 'bottom', 
 *    ),
 *    'colorstops' => array(
 *      '1' => '25%', 
 *      '2' => '25%', 
 *      '3' => '25%', 
 *      '4' => '25%', 
 *    ),
 *  ),
 * );
 * 
 * !!! For CUSTOM_OPTION_MACHINE_NAME you must use alphanumeric characters !!!
 * 
 * For radial gradients use the following structure.
 * You can add as much colorstops as you want, keep incremental numeration of
 * the colorstops array elements. The colorstop values will change the gradients
 * transitions - only use percentages.
 * 
 * Also see: https://developer.mozilla.org/en/CSS/radial-gradient
 * 
 * $spctrm_cfg['options']['CUSTOM_OPTION_MACHINE_NAME'] = array(
 *  'title' => 'CUSTOM TITLE',
 *  'element' => 'AFFECTED HTML ELEMENT',
 *  'gradient' => array (
 *    'meta' => array(
 *      'type' => 'radial',
 *      'position' => 'center', 
 *      'shape' => 'circle',
 *      'size' => '',
 *      'extent-keyword' => 'closest-side',
 *    ),
 *    'colorstops' => array(
 *      '1' => '', 
 *      '2' => '', 
 *      '3' => '', 
 *      '4' => '', 
 *    ),
 *  ),
 * );
 * 
 * !!! For CUSTOM_OPTION_MACHINE_NAME you must use alphanumeric characters !!!
 * 
 */

$spctrm_cfg['options']['h1'] = array(
  'title' => 'Headline H1',
  'element' => 'h1',
);
$spctrm_cfg['options']['a'] = array(
  'title' => 'Hyperlinks A',
  'element' => 'a',
);
$spctrm_cfg['options']['body'] = array(
  'title' => 'General BODY Color',
  'element' => 'body',
);
$spctrm_cfg['options']['p'] = array(
  'title' => 'General Paragraph Color',
  'element' => 'p',
);
$spctrm_cfg['options']['footer'] = array(
  'title' => 'Footer Gradient',
  'element' => 'div#footer-wrapper',
  'gradient' => array (
    'meta' => array(
      'type' => 'radial',
      'position' => 'center', 
      'shape' => 'circle',
      'size' => '',
      'extent-keyword' => 'farthest-corner',
    ),
    'colorstops' => array(
      '1' => '', 
      '2' => '', 
      '3' => '', 
      '4' => '', 
    ),
  ),
);
$spctrm_cfg['options']['header'] = array(
  'title' => 'Header Gradient',
  'element' => 'div#header',
  'gradient' => array (
    'meta' => array(
      'type' => 'linear',
      'to' => 'bottom',
    ),
    'colorstops' => array(
      '1' => '', 
      '2' => '', 
    ),
  ),
);
$spctrm_cfg['options']['headeretc'] = array(
  'title' => 'Header Link Color',
  'element' => 'div#header a',
);

/**
 * Now set up the explicit color schemes using the following structure.
 * You can use any CSS color property as CUSTOM_COLOR_PROPERTY e.g.:
 * color, border-color, background-color, outline-color, etc.
 * 
 * For gradients use the second notation shown below.
 * 
 * Also see the pre-configuration for explicit examples!
 * 
 * 
 *  $spctrm_cfg['schemes']['SCHEME_MACHINE_NAME'] = array(
 * 
 *    '#title' => 'SCHEME_TITLE',
 * 
 *    'CORRESPONDING_CUSTOM_OPTION_MACHINE_NAME' => array(
 *      'CSS_COLOR_PROPERTY' => 'ffffff',
 *    ),
 * 
 *    'CORRESPONDING_CUSTOM_OPTION_MACHINE_NAME' => array(
 *      'colorstop' => array(
 *        '1' => 'ffffff',
 *        '2' => '111111',
 *        '3' => '666666',
 *        '4' => '000000',
 *      ),
 *    ),
 *  
 *  );
 * 
 */

// -----------------------------------------------------------------------------
// DEFAULT SCHEME, EDIT BUT DON'T DELETE:
// $spctrm_cfg['schemes']['default'] IS A MUST!
// -----------------------------------------------------------------------------

$spctrm_cfg['schemes']['default'] = array(
  '#title' => 'Default Scheme',
  'h1' => array(
    'background-color' => 'ffffff',
    'color' => '000000',
  ),
  'a' => array(
    'border-color' => '000000',
    'color' => 'f0f0f0',
  ),
  'body' => array(
    'background-color' => '000000',
  ),
  'p' => array(
    'color' => '000000',
  ),
  'footer' => array(
    'colorstop' => array(
      '1' => 'ffffff',
      '2' => '111111',
      '3' => '666666',
      '4' => '000000',
    ),
  ),
  'header' => array(
    'colorstop' => array(
      '1' => 'ffffff',
      '2' => '111111',
    ),
  ),
  'headeretc' => array(
    'color' => '0ccaa1',
    'background-color' => '0ccaa1',
  ),
);

// -----------------------------------------------------------------------------
// CUSTOM SCHEMES:
// -----------------------------------------------------------------------------

$spctrm_cfg['schemes']['example'] = array(
  '#title' => 'EXAMPLE',
  'h1' => array(
    'background-color' => 'ffffff',
    'color' => '000000',
  ),
  'a' => array(
    'border-color' => '000000',
    'color' => 'f0f0f0',
  ),
  'body' => array(
    'background-color' => '000000',
  ),
  'p' => array(
    'color' => '00ff00',
  ),
  'footer' => array(
    'colorstop' => array(
      '1' => 'ff00ff',
      '2' => '11ff11',
      '3' => '660066',
      '4' => '00ff00',
    ),
  ),
  'header' => array(
    'colorstop' => array(
      '1' => 'ff99ff',
      '2' => 'cc1111',
    ),
  ),
  'headeretc' => array(
    'color' => '111000',
    'background-color' => '000000',
  ),
);
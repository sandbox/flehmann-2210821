<div id="spctrm-container" class="spctrm-hide">
  <div class="spctrm-inner">
    <?php if(!empty($spctrm)) :?>
      <?php print render($spctrm['spctrm_form']); ?>
    <?php endif; ?>
  </div>
  <div class="spctrm-toggle"></div>
  <div class="clearfix"></div>
</div>
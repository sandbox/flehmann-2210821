################################################################################
# 									       #
#   SPCTRM - THE ALTERNATIVE DRUPAL COLOR MODULE                               #
# 									       #################################################################################


CONTENT:	INTRO
		INSTALLATION
		CONFIGURATION
		AUTHOR
		SPONSOR


--------------------------------------------------------------------------------
| INTRO                                                                        | 
-------------------------------------------------------------------------------- SPCTRM is an alternative to the Drupal color core module. Therefore SPCTRM provides another and more flexible approach than the color core 
module. You can change the schemes easily using the SPCTRM toolbar after you 
applied them within the theme's directory. Each color can be changed lightning 
fast and intuitive as well using the toolbar's jquery colorpicker.

Have a look on the benefits:

* Toolbar - No more backend color adjusting
* Live Preview - See the changes instantly
* Flexibility - Change the color of single elements
* Time savings - No extra coding for a embedded preview within the backend
* Gradients - Radial and linear supported!
Download it now and statisfy yourself of the simplicity and flexibility 
SPCTRM provides to you!


--------------------------------------------------------------------------------
| INSTALLATION                                                                 | 
--------------------------------------------------------------------------------
1. 	Download SPCTRM and extract it to the directory sites/all/modules/
2. 	Activate SPCTRM
3. 	Copy the folder named config-sample from the module's folder to your 
	theme's folder and rename it to spctrm so you will find it under 
	yourtheme/spctrm
4. 	Open spctrm.inc within the prior renamed folder and change the schemes 
	as you want
	
For further configuration instructions see the point „configuration“--------------------------------------------------------------------------------
| CONFIGURATION                                                                | 
--------------------------------------------------------------------------------Have a look on the Screenshot within the module's folder, it describes the 
configuration file very well. 
Also you will find a pre configuration and a lot of comments inside of spctrm.inc
(yourtheme/spctrm/spctrm.inc; see installation step 4)--------------------------------------------------------------------------------
| AUTHOR                                                                       | 
--------------------------------------------------------------------------------Florian Lehmann - Interactive Media Designer and Developer--------------------------------------------------------------------------------
| SPONSOR                                                                      | 
--------------------------------------------------------------------------------Tojio GmbH - Full Service Internet Agency

http://tojio.com
/**
 * @file
 * SPCTRM javascript
 * 
 * Provides animations like SPCTRM toolbar toggle and own fieldset toggle to 
 * avoid window scrolling.
 * Implements the ability to change the color.
 * 
 */

(function ($) {

Drupal.behaviors.spctrm = {};

Drupal.behaviors.spctrm.attach = function (context, settings) {
  
  var affectedElements;
  var affectedElementsName;
  var allGradientColors;
  var changeOption;
  var colorAttributes;
  var spctrmOptions;
  var colorPickerName;
  var colorPickerParent;
  var colorScheme;
  var colorSet;
  var cssAttribute;
  var gradientObject;
  var gradientTemplate;
  var hexValue;
  var hexValueChanged;
  var hexValueParent;
  var schemeElement;
  var selectValue;

  spctrmOptions = Drupal.settings.spctrm.options;

  // Function will be called when colorpicker gets clicked
  $('div.color_picker', context).click(function () {
    
    colorPickerParent     = $(this).parents('div.form-type-jquery-colorpicker');
    hexValueParent        = $(colorPickerParent).prev('div.form-type-textfield');
    hexValue              = $(hexValueParent).children('input.hex-value-view');
    affectedElementsName  = $(hexValueParent).parent().prevAll('input.elements').attr('name');
    affectedElements      = $(hexValueParent).parent().prevAll('input.elements').val(); 
    cssAttribute          = $(hexValueParent).prevAll('input.attributes').val();
    gradientObject        = spctrmOptions[affectedElementsName]['gradient'];
    
    // Check if it's a gradient
    if (typeof(gradientObject) === 'object') {
      
      $(this).children('input.form-colorpicker').change(function () { //on change colorpicker
        setSchemeToCustom(); // Switch scheme selection to 'custom' value
        hexValueChanged = $(this).val(); // Get the changed hex value
        $(hexValue).val(hexValueChanged); // Write back new hex value to hex value input
        allGradientColors = $(hexValueParent).parent('div.spctrm-row').find('.form-type-textfield .hex-value-view'); // Get all gradient colors
        resetGradientTemplate();
        getGradientType();
        createGradientTemplate(1);
        drawGradient(); 
      });
      
    }
    else {
      
      $(this).children('input').change(function () {
        setSchemeToCustom(); // Switch scheme selection to 'custom' value
        hexValueChanged = $(this).val(); // Get the changed hex value
        $(affectedElements).css(cssAttribute, '#' + hexValueChanged); // Instant change of hex value on affected elements
        $(hexValue).val(hexValueChanged); // Write back new hex value to hex value input
      });
      
    }
    
  });
  
  // Function will be called when scheme selection gets changed
  $('select#edit-spctrm-scheme', context).change(function() {
    
    selectValue = $(this).val();
    colorScheme = Drupal.settings.spctrm.schemes[selectValue]; 
    
    // If clause avoids selection of non existent scheme
    if (colorScheme != undefined) {
      // First remove #title from scheme object and iterate through it
      delete colorScheme['#title'];
      for (schemeElement in colorScheme) {

        colorSet = colorScheme[schemeElement]; // Get color set from settings
        gradientObject = spctrmOptions[schemeElement]['gradient']; // Get gradient object
        
        // Iterate through colorSet
        for (colorAttributes in colorSet) {
          

          // Colorstop implicates a gradient, so draw a gradient
          if (colorAttributes === 'colorstop') {
            allGradientColors = colorSet[colorAttributes];
            resetGradientTemplate();
            getGradientType();
            createGradientTemplate(0);
            drawGradient();
          }
          else {
            // Change colorpicker colors
            colorPickerName = 'div#edit-' + schemeElement + '-' + colorAttributes + '-colorpicker-inner_wrapper div.color_picker';
            $(colorPickerName).css('background-color', '#' + colorSet[colorAttributes]);
            $(colorPickerName).ColorPickerSetColor(String(colorSet[colorAttributes]));
            $('input#edit-' + schemeElement + '-' + colorAttributes).val(String(colorSet[colorAttributes]));
            $(colorPickerName).find('input.form-colorpicker').val(String(colorSet[colorAttributes]));

            // Change the CSS
            affectedElements = $(colorPickerName).parents('.form-type-jquery-colorpicker').parent().prevAll('.elements').val();
            cssAttribute = $(colorPickerName).parents('.form-type-jquery-colorpicker').prevAll('.attributes').val();
            $(affectedElements).css(cssAttribute, '#' + colorSet[colorAttributes]);
          }

        }
      }
    }
  });
  
  function setSchemeToCustom() {
    $('select#edit-spctrm-scheme').val('custom');
  }
  
  function createGradientTemplate(changeOption) {
    // changeOption === 1 on change colorpicker  
    // changeOption === 0 on change schemeset
    var cnt = 0;
    
    if (changeOption === 1) {
      allGradientColors.each(function (cnt) {
        cnt++;
        gradientTemplate += ', #' + $(this).val() + ' ' + gradientObject['colorstops'][cnt];
      });
    }
    else {
      allGradientColors = colorSet[colorAttributes];
      for (var gradientColor in allGradientColors) {
        cnt++;
        gradientTemplate += ', #' + allGradientColors[gradientColor] + ' ' + gradientObject['colorstops'][cnt];
        colorPickerName = 'div#edit-' + schemeElement + '-' + colorAttributes + cnt + '-colorpicker-inner_wrapper div.color_picker';
        $(colorPickerName).css('background-color', '#' + allGradientColors[gradientColor]);
        $(colorPickerName).ColorPickerSetColor(String(allGradientColors[gradientColor]))
        $('input#edit-' + schemeElement + '-' + colorAttributes + cnt).val(String(allGradientColors[gradientColor]));
        $('input#edit-' + schemeElement + '-' + colorAttributes + cnt + "-colorpicker").val(String(allGradientColors[gradientColor]));
      }
      affectedElements = $(colorPickerName).parents('.form-type-jquery-colorpicker').parent().prevAll('.elements').val();
    } 
  }
  
  // Reset the gradient template
  function resetGradientTemplate() {
    gradientTemplate = '';
  }
  
  // Get the gradient type from settings
  function getGradientType() {
    if(gradientObject['meta']['type'] === 'linear') {
      gradientTemplate = gradientObject['meta']['to'];
    }
    else {
      // else => radial
      gradientTemplate = gradientObject['meta']['position'] + ', ' + gradientObject['meta']['shape'] + ' ' + gradientObject['meta']['extent-keyword'];
    }
  }
  
  // Draws the CSS3 Gradient
  function drawGradient() {
    $(affectedElements).css('background', gradientObject['meta']['type'] + '-gradient(' + gradientTemplate  + ')');
    $(affectedElements).css('background', '-o-' + gradientObject['meta']['type'] + '-gradient(' + gradientTemplate  + ')');
    $(affectedElements).css('background', '-moz-' + gradientObject['meta']['type'] + '-gradient(' + gradientTemplate  + ')');
    $(affectedElements).css('background', '-webkit-' + gradientObject['meta']['type'] + '-gradient(' + gradientTemplate  + ')');
    $(affectedElements).css('background', '-ms-' + gradientObject['meta']['type'] + '-gradient(' + gradientTemplate  + ')');
  }
  
  // Sidebar toggle
  $('.spctrm-toggle', context).click(function () {
    $('#spctrm-container').toggleClass('spctrm-hide');
    $(this).addClass('toggle-show');
  });
  
  // Fieldset toggle
  $('.spctrm-collapse legend', context).click(function () {
    $(this).next('.fieldset-wrapper').slideToggle('fast');
    $(this).parent('.spctrm-collapse').toggleClass('spctrm-collapsed');
  }); 
  
};

}(jQuery));